import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class ClashOfEgalGUI extends JFrame {
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClashOfEgalGUI frame = new ClashOfEgalGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ClashOfEgalGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 250, 519);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		JLabel lblClashOfEgal = new JLabel("Clash of Egal");
		panel.add(lblClashOfEgal, BorderLayout.NORTH);
		
				JButton btnSchliessen = new JButton("->");
				panel.add(btnSchliessen, BorderLayout.EAST);
				
						JPanel pnl_Einheitenübersicht = new JPanel();
						panel.add(pnl_Einheitenübersicht, BorderLayout.CENTER);
						pnl_Einheitenübersicht.setLayout(new GridLayout(4, 0, 0, 0));
						
								JLabel lblName = new JLabel("Name");
								pnl_Einheitenübersicht.add(lblName);
								
										JLabel lblBild = new JLabel("");
										pnl_Einheitenübersicht.add(lblBild);
										
												JLabel lblKosten = new JLabel("Gold");
												pnl_Einheitenübersicht.add(lblKosten);
												
														JButton btnKaufen = new JButton("Kaufen");
														pnl_Einheitenübersicht.add(btnKaufen);
				btnSchliessen.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Einheit e1 = Einheitenspeicher.next();
						lblName.setText(e1.getName());
						lblKosten.setIcon(new ImageIcon("./src/Gold.png"));
						lblKosten.setText(e1.getKosten() + "");
						lblBild.setIcon(new ImageIcon(e1.getBild()));
					}
				});
	}

}
