package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class SQLDatabase {

	private final String driver = "com.mysql.jdbc.Driver";
	private final String url = "jdbc:mysql://localhost/rechtecke?"; // ? <-- muss immer da sein
	private final String user = "root";
	private final String password = "";

	public void rechteckEintragen(Rechteck r) {
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);

			String sql = "INSERT INTO T_rechtecke (x, y, breite, hoehe) VALUES\r\n" + "(?,?,?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, r.getX());
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreite());
			ps.setInt(4, r.getHoehe());

			// auch bei einem INSERT immer executeUpdate
			ps.executeUpdate();

			// Datenbanken k�nnen nur eine begrenzte Anzahl von Connections �ffnen
			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Rechteck> getAlleRechtecke() {
		List<Rechteck> rechtecke = new LinkedList<Rechteck>();
		try {
			
			// JDBC-Treiber laden
			Class.forName(driver);
			
			// Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			
			String sql = "SELECT * FROM T_rechtecke;";
			Statement s = con.createStatement();
			
			//existiert solange, bis die Connection nicht mehr da ist
			ResultSet rs = s.executeQuery(sql); 
			
			//liefert Info, ob es noch einen neuen Datensatz gibt
			while (rs.next()) {  
				
				// es wird bei 2 angefangen zu z�hlen, Rechteck_id
				int x = rs.getInt(2); 
				int y = rs.getInt("Y");
				int breite = rs.getInt("Breite");
				int hoehe = rs.getInt("Hoehe");
				
				//kommt in die Linkedlist
				rechtecke.add(new Rechteck(x, y, breite, hoehe));
				return rechtecke;
			}
				
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rechtecke;
	}
}
