package model;

import java.util.Random;

public class Rechteck {

	private int x;
	private int y;
	private int breite;
	private int hoehe;

	public Rechteck() {
		super();
		this.x = 0;
		this.y = 0;
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.x = x;
		this.y = y;
		this.breite = breite;
		this.hoehe = hoehe;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}

	//Methoden, Push wurde f�r HA am 8.3.2019 vergessen
	public boolean enthaelt(Punkt p) {
		return this.enthaelt(p.getX(), p.getY());
	}
	
	public boolean enthaelt(int x, int y) {
		if (((x <= this.getX() + this.getHoehe()) && (x >= this.getX())) && ((y <= this.getY() + this.getHoehe()) && (y >= this.getY()))) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean enthaelt(Rechteck rechteck) {
		return this.enthaelt(rechteck.getX(), rechteck.getY())  && this.enthaelt(rechteck.getX() + rechteck.getBreite(), rechteck.getY() + rechteck.getHoehe());
	}
	
	public static Rechteck generiereZufallsRechteck() {
        Random random = new Random();
        int x = random.nextInt(1200);
        int y = random.nextInt(1000);
        int breite = random.nextInt(1200-x);
        int hoehe = random.nextInt(1000-y);
        return new Rechteck(x, y, breite, hoehe);
    }
	
	@Override
	public String toString() {
		return "Rechteck [x=" + x + ", y=" + y + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}
}
