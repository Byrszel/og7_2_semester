package view;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Eingabemaske extends JFrame {

	private JPanel contentPane;
	private JTextField x_Textfield;
	private JTextField y_Textfield;
	private JTextField breite_Textfield;
	private JTextField hoehe_Textfield;
	private JButton btnAdd;
	private JLabel lblAdd;
	private BunteRechteckeController brc;

	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eingabemaske frame = new Eingabemaske(new BunteRechteckeController());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @param bunteRechteckeController
	 */
	
	public Eingabemaske(BunteRechteckeController brc) {
		this.brc = brc;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		// x-koordinate
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel lblNewLabel = new JLabel("X:");
		panel.add(lblNewLabel);

		x_Textfield = new JTextField();
		panel.add(x_Textfield);
		x_Textfield.setColumns(10);

		// y-koordinate
		JLabel lblNewLabel_1 = new JLabel("Y:");
		panel.add(lblNewLabel_1);

		y_Textfield = new JTextField();
		panel.add(y_Textfield);
		y_Textfield.setColumns(10);

		// breite
		JLabel lblNewLabel_2 = new JLabel("Breite:");
		panel.add(lblNewLabel_2);

		breite_Textfield = new JTextField();
		panel.add(breite_Textfield);
		breite_Textfield.setColumns(10);

		// h�he
		JLabel lblNewLabel_3 = new JLabel("H\u00F6he:");
		panel.add(lblNewLabel_3);

		hoehe_Textfield = new JTextField();
		panel.add(hoehe_Textfield);
		hoehe_Textfield.setColumns(10);

		// text links vom button
		lblAdd = new JLabel("Bestaetigen ->");
		panel.add(lblAdd);

		// button
		btnAdd = new JButton("Hinzufuegen");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addButtonSaved();
			}
		});
		panel.add(btnAdd);
		setVisible(true);
	}

	protected void addButtonSaved() {
		int x = Integer.parseInt(x_Textfield.getText());
		int y = Integer.parseInt(y_Textfield.getText());
		int breite = Integer.parseInt(breite_Textfield.getText());
		int hoehe = Integer.parseInt(hoehe_Textfield.getText());
		Rechteck r = new Rechteck(x, y, breite, hoehe);
		brc.add(r);
	}
}
