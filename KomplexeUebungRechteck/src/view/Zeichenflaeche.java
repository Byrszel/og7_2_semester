package view;

import controller.BunteRechteckeController;

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;
							//Vererbung
public class Zeichenflaeche extends JPanel {
	
	//Attribute
	private BunteRechteckeController brc;
	
	//Konstruktoren
	public Zeichenflaeche(BunteRechteckeController brc){
		this.brc = brc;	
	}
	
	@Override
	public void paintComponent(Graphics g) {
		
		//Rechteck zeichnen
		g.setColor(Color.BLACK);
		//Rechteck an der oberen Linke entfernt
		
		//Abfangen der Rechtecke
		for (int i = 0; i < brc.getRechtecke().size(); i++) {
			
			int x = brc.getRechtecke().get(i).getX();	
			int y = brc.getRechtecke().get(i).getY();
			int breite = brc.getRechtecke().get(i).getBreite();
			int hoehe = brc.getRechtecke().get(i).getHoehe();
			
			//Ausgabe
			g.drawRect(x,y,breite,hoehe);
			
		}
	}
}
