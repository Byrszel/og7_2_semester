package controller;

import java.util.LinkedList;
import java.util.List;

import model.Rechteck;
import model.SQLDatabase;
import view.Eingabemaske;

public class BunteRechteckeController {

	private List<Rechteck> rechtecke;
	private SQLDatabase database;

	public BunteRechteckeController() {
		super();
		rechtecke = new LinkedList<Rechteck>();
		this.database = new SQLDatabase();
		rechtecke = this.database.getAlleRechtecke();
	}

	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
		this.database.rechteckEintragen(rechteck);
	}

	public void reset() {
		rechtecke.clear();
	}

	public List<Rechteck> getRechtecke() {
		return rechtecke;
	}

	public void setRechtecke(List<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

	public void generiereZufallsRechtecke(int anzahl) {
		this.reset();
		for (int i = 0; i < anzahl; i++) {
			this.add(Rechteck.generiereZufallsRechteck());
		}
	}

	public static void main(String[] args) {

	}

	public void rechteckHinzufuegen() {
		new Eingabemaske(this);
	}

}
